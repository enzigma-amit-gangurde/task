const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const Task = require('./task')

const userSchema = new mongoose.Schema({
    name : {
        type : String,
        required : true,
        trim : true
    },
    email : {
        type : String,
        required : true,
        unique :true,
        trim : true,
        lowercase : true,
        validate(value){
            if(!validator.isEmail(value)){
                throw new Error('Email is invalid')
            }
        }
    },
    Password :{
        type : String,
        required :true,
        trim : true,
        minlength : 6,
        validate(value){
            if(value.toLowerCase().includes("password")){
                throw new Error('Password cannot contain "Password"')
            }
        }
    },
    age : {
        type : Number,
        default : 0,
        Validate(vale){
            if(value < 0){
                throw new Error ('Age can not be negative')
            }
        }
    },
    tokens : [{
        token : {
            type : String,
            required : true
        }
    }],
    avatar:{
        type:Buffer
    }   
},
{

timestamps:true

})

userSchema.virtual('tasks',{
    ref:'Task',
    localField:'_id',
    foreignField:'owner'
}) 

userSchema.methods.toJSON=function(){
const user=this;
const userObject=user.toObject();
delete userObject.Password;
delete userObject.tokens;
return userObject;


}





userSchema.methods.generateToken = async function () { 
    const user = this
    const token = jwt.sign({ _id : user._id.toString() }, 'thisismysign')
    user.tokens = user.tokens.concat({ token })
    await user.save()
    return token
}

userSchema.statics.findCredentials = async(email, Password) => {
    const user = await User.findOne({email : email})
    if(!user){
        throw new Error('Cant Find User')
    }
    const isMatch = await bcrypt.compare(Password, user.Password)
    if(!isMatch){
        throw new Error('Wrong Password')
    }
    return user
}

//Hashing the Password before Saving
userSchema.pre('save', async function (next) {
    const user = this

    if(user.isModified('Password')){
        user.Password = await bcrypt.hash(user.Password, 8)
    }
    next()

})

//Delete user task when user is removed

userSchema.pre('remove',async function(next){
    const user=this
    await Task.deleteMany({owner:user._id})
    next()
})

const User = mongoose.model('User', userSchema)
module.exports = User
