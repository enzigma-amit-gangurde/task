const jwt=require('jsonwebtoken');
const User=require('../models/users');

const auth=async(req,res,next)=>{
try {
    const token=req.header('Authorization').replace('Bearer ','');
    const decoded=jwt.verify(token,'thisismysign');
    const user=await User.findOne({_id:decoded._id,'tokens.token':token});
    if(!user)
    {
        throw Error('User Not Present');
    }
    req.token=token
    req.user=user
   next();
    
} catch (error) {
    res.send(error);
    
}

}

module.exports=auth