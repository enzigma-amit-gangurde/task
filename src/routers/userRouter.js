const express=require('express');
const router=express.Router();
const User=require('../models/users');
const { update } = require('../models/users');
const auth=require('../middleware/auth');
const multer=require('multer');
const { Error } = require('mongoose');


//upload image of any type
const upload=multer({
       limits:{
    fileSize:1000000
   },
   fileFilter(req,file,cb)
   {
       if(!file.originalname.match(/\.(jpg|jpeg|png)$/))
       {
           return cb(new Error('Please upload an image'))
       }
       cb(undefined,true);
   }
})
const errormiddleware=(req,res,next)=>{
    throw new Error('From my middleware');   
}

router.post('/users/me/avatar',auth,upload.single('avatar'),async(req,res)=>{
    req.user.avatar=req.file.buffer
    await req.user.save();
res.send('Uploaded Succesfully');
},(error,req,res,next)=>
{
    res.status(400).send({error:error.message});

})

//delete avatar

router.delete('/users/me/avatar',auth,async(req,res)=>{
req.user.avatar=undefined;
await req.user.save();
res.send('Data Deleted Successfully');

})

//get avatar by id

router.get('/users/:id/me',auth,async(req,res)=>{
     
    try {
        
        const user=User.findById(req.params.id);
        if(!user || !user.avatar)
        {
            throw new Error();
        }
        res.set('Content-Type','images/jpg');
        res.send(user.avatar);


    } catch (error) {
      res.status(404).send();
    }

})

//upload File
const imageUpload=multer(
    {
        dest:'avatars',
    limits:
    {
        fileSize:1000000
    },
    fileFilter(req,file,cb)
    {
        if(!file.originalname.match(/\.(doc|docx)$/)){
           return cb(new Error('Please upload a Word Document'))

        }    
        cb(undefined,true);       
    }  
})
router.post('/users/me/avator',auth,imageUpload.single('avator'),async(req,res)=>{
    req.user.avatar=req.file.buffer
    await req.user.save();
    res.send(); 
},(error,req,res,next)=>{
    res.status(400).send({error:error.message})

})



// get all users

router.get('/users',async (req, res) => {  

    try{
          const user = await User.find({})
          res.status(200).send(user)
    }catch(e){
          res.status(400).send()
    }
})

//get my details only

router.get('/users/me',auth,async(req,res)=>{
    res.send(req.user);
})



router.post('/users',async(req, res) => {   
    const user = new User(req.body)
    try{
        await user.save()
        const token = await user.generateToken()
        res.status(201).send( {user, token} )   

    }catch(e){
        res.status(400).send(e)
    }    
    
})

//

router.post('/users/login',async(req, res) => {
    try {
        //findCredentials function defined in the model User
        const user = await User.findCredentials(req.body.email, req.body.Password)
        const token = await user.generateToken()
        res.send({user,token})

    } catch(error){
        res.status(400).send({error:'Unable to Login please retry'})
    }
})


//User logout

router.post('/users/logout',auth,async(req,res)=>{

try {
    req.user.tokens=req.user.tokens.filter((token)=>{
        return token.token !== req.token
    })
    
    await req.user.save();
    res.send();
} catch (error) {    
    res.status(500).send();
}

})


//logout AllUser

router.post('/users/logoutAll',auth,async(req,res)=>{

    try {
        req.user.tokens=[];
        await req.user.save();
        res.send();
        
    } catch (error) {
        res.status(500).send();
        
    }

})




router.get('/users/:id', async (req, res) => {

   const _id = req.params.id

    try{
        const user = await User.findById(_id)
        if(!user){
            res.status(400).send()
        }
        res.status(200).send(user)
    }catch(e){
        res.status(500).send()
    }
 
})


//Update user by perticular id

router.patch('/users/me',auth,async(req, res) => {

    const updates = Object.keys(req.body) 
    const allowUpdates = ["name", "email", "Password", "age"] 
    const isValidOperation = updates.every((update) => allowUpdates.includes(update))

    if(!isValidOperation){
        return res.status(400).send({error : "Invalid Operation"})
    }
        try{

            updates.forEach((update) => req.user[update] = req.body[update])
            await req.user.save();        
            
            res.send(req.user);
        }catch(error){
            res.status(500).send(error)
        }

})


router.delete('/users/me',auth, async(req, res) => {
        try{
            // const user = await User.findByIdAndDelete(req.user._id)

            // if(!user){
            //     res.status(404).send(user)
            // }
            // res.status(200).send()

            await req.user.remove();
            
            res.send(req.user);

 
        }catch(error){
            res.status(500).send(error)
        }
})
module.exports = router
