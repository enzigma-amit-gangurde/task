const express=require('express');
const router=express.Router();
const Task=require('../models/task')
const auth=require('../middleware/auth');

//post data into tasks

router.post('/tasks',auth,async(req, res) => {
    //const tasks = new Task(req.body)
      const task=new Task({
            ...req.body,
          owner:req.user._id
      })
    try{
        await task.save()
        res.status(201).send(task)
    }catch(e){
        res.status(400).send(e)
    }
   
})

//get all data from tasks

router.get('/tasks',auth,async(req, res) => {
  
   const match={};  
   if(req.query.completed)
   {
       match.completed=req.query.completed==="true"
   }  

      try{    
        await req.user.populate({
            path:'tasks',
            match,
            options:{
                limit:parseInt(req.query.limit),
                skip:parseInt(req.query.skip),            
            }
        }).execPopulate()
        res.status(200).send(req.user.tasks);
      }catch(e){
        res.send(400).send(e)
      }
    
})



//get data with perticular id

router.get('/tasks/:id', auth,async(req, res) => {
    const _id = req.params.id

     try{ 
            //const task = await Task.findById(_id)
            const task=await Task.findOne({_id,owner:req.user._id});

            if(!task){
                res.status(400).send()
            }
            res.status(200).send(task)
     }catch(e){
        res.status(500).send(e)
     }
   

})


//update perticular tasks by id


router.patch('/tasks/:id', auth,async(req, res) => {
   
        const tasks = Object.keys(req.body);
        const allowTaskUpadte = ["completed","description"]
        const isValidOpeartion = tasks.every((task) => allowTaskUpadte.includes(task))

        if(!isValidOpeartion){
            return res.status(404).send({'error' : 'Invalid Operation'})
        }

    try{
      
        const task=Task.findOne({_id:Lreq.params.id,owner:req.user._id})      
        tasks.forEach((tsk)=>(task[tsk]=req.body[tsk]));            
        await task.save();                     
        res.status(200).send(task)
       
    }catch(error){
        res.status(500).send(error)
    }
})



//delete task by id

router.delete('/tasks/:id', auth,async(req, res) => {   
    
    try{
       // const task = await Task.findByIdAndDelete(req.params.id)
       const task=await Task.findOne({_id:req.params.id,owner:req.user._id})
        if(!task){
            res.status(404).send()
        }
        res.status(200).send(task)
    }catch(error){
        res.status(500).send();
    }
})

module.exports = router
